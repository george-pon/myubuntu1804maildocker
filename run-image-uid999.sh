#!/bin/bash
#
# test run image
#
function docker-run-myubuntu1804maildocker() {
    IMAGE_NAME=${PREFIX}$(awk '/^ENV MYUBUNTU1804MAILDOCKER_IMAGE/ {print $3;}' Dockerfile)

    ${WINPTY_CMD} docker run -i -t --rm \
        -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e no_proxy="${no_proxy}" \
        -u 999 \
        ${IMAGE_NAME}:latest
}
docker-run-myubuntu1804maildocker
