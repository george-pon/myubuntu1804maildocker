#!/bin/bash

set -e
set -x

if [ -n "$HOME" ] ; then
    if [ -r $HOME/.bashrc ] ; then
        source $HOME/.bashrc
    fi
fi

# read options and set environment variable
while true
do
    if [ x"$1"x = x"-e"x ]; then
        eval "$2"
        export ${2%%=*}
        mkdir -p /etc/profile.d
        echo "export $2" >> /etc/profile.d/docker-entrypoint-env.sh
        shift
        shift
        continue
    fi
    break
done

# add sudo user
# https://qiita.com/iganari/items/1d590e358a029a1776d6 Dockerコンテナ内にsudoユーザを追加する - Qiita
# ユーザー名 MYOS_USER
# グループ名 MYOS_GROUP
# パスワード MYOS_PASSWORD
# ユーザーＩＤ MYOS_UID
# グループＩＤ MYOS_GID
if [ "$(id -u)" = '0' ]; then
    if [ -n "$MYOS_USER" -a -n "$MYOS_PASSWORD" ]; then
        if [ -z "$MYOS_GROUP" ]; then
            MYOS_GROUP=$MYOS_USER
        fi
        if [ -n "$MYOS_UID" ]; then
            MYOS_UID_OPT="--uid ${MYOS_UID}"
        else
            MYOS_UID_OPT=
        fi
	if [ -n "$MYOS_GID" ] ; then
            MYOS_GID_OPT="--gid ${MYOS_UID}"
	else
            MYOS_GID_OPT=
	fi
	
        groupadd ${MYOS_GID_OPT} ${MYOS_GROUP}
        useradd  -g ${MYOS_GROUP} ${MYOS_UID_OPT} -G sudo -m -s /bin/bash ${MYOS_USER}
        echo "${MYOS_USER}:${MYOS_PASSWORD}" | chpasswd
        echo "Defaults visiblepw"            >> /etc/sudoers
        echo "${MYOS_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

        # copy setting files
        mkdir -p /home/$MYOS_USER
        for i in .bashrc .bash_profile .vimrc .emacsrc
        do
            /bin/cp /root/$i  /home/$MYOS_USER/
        done
        chown -R ${MYOS_USER}:${MYOS_GROUP} /home/${MYOS_USER}
        export HOME=/home/$MYOS_USER

        # use normal user ubuntu
        exec gosu $MYOS_USER bash
    fi
fi

if [ $# -eq 0 ]; then
    exec /bin/bash
else
    "$@"
    RC=$? ; exit $RC
fi
