#!/bin/bash
#
# test run image
#
function docker-run-myubuntu1804maildocker() {
    IMAGE_NAME=${PREFIX}$(awk '/^ENV MYUBUNTU1804MAILDOCKER_IMAGE/ {print $3;}' Dockerfile)

    docker pull registry.gitlab.com/george-pon/${IMAGE_NAME}:latest
    ${WINPTY_CMD} docker run -i -t --rm \
        -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e no_proxy="${no_proxy}" \
        registry.gitlab.com/george-pon/${IMAGE_NAME}:latest
}
docker-run-myubuntu1804maildocker
